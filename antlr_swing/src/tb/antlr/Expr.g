grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat )+ EOF!;

stat
    : expr NL -> expr
    
    | VAR ID PODST expr NL -> ^(VAR ID) ^(PODST ID expr)
    | VAR ID NL -> ^(VAR ID)
    | ID PODST expr NL -> ^(PODST ID expr)
    | PRINT expr NL -> ^(PRINT expr)
    | if_stmt NL -> if_stmt
    | NL ->
    ;
    
if_stmt
    : IF^ expr THEN! expr (ELSE(expr))?
    ;
expr
    : multExpr
      ( PLUS^ multExpr
      | MINUS^ multExpr
      | LO^ multExpr
      | HI^ multExpr
      | LOEQ^ multExpr
      | HIEQ^ multExpr
      | EQ^ multExpr
      | NE^ multExpr
      )*
    ;
  
multExpr
    : atom
      ( MUL^ atom
      | DIV^ atom
      | MOD^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;

VAR :'var';

IF  : 'if';

ELSE: 'else';

THEN: 'then';

PRINT: 'print';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;

LP
	:	'('
	;

RP
	:	')'
	;

PODST
	:	'='
	;

PLUS
	:	'+'
	;

MINUS
	:	'-'
	;

MUL
	:	'*'
	;

DIV
	:	'/'
	;

MOD
  : '%'
  ;

HI
  : '>'
  ;
  
LO
  : '<'
  ;

HIEQ
  : '>='
  ;
  
LOEQ
  : '<='
  ;
  
EQ
  : '=='
  ;
  
NE
  : '!='
  ;
  
