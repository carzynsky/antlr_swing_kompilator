tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer if_cnt = 0;
}

prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje>start: 
<name;separator=\" \n\"> ";

// Deklaracja zmiennej
decl  :
        ^(VAR i1=ID) {globals.newSymbol($i1.text);} -> dek(n={$i1.text})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}
    

// Ustawienie wartości zmiennej NOT WORKING => template is not dislayed
//set   : 
//        ^(PODST i1=ID  e2=expr) {globals.setSymbol($i1.text, getInt($e2.text));} -> przypisanie(n={$i1.text}, p1={$e2.st})
//    ;
//    catch [RuntimeException ex] {errorID(ex,$i1);}
    
// Pobranie wartości zmiennej
//get :
//      ID {getSymbol($ID.text);} -> wczytaj(n={$ID.text})
//    ;

expr    : ^(PLUS  e1=expr e2=expr)  -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr)  -> odejmij(p1={$e1.st}, p2={$e2.st})
        | ^(MUL   e1=expr e2=expr)  -> pomnoz(p1={$e1.st}, p2={$e2.st})
        | ^(DIV   e1=expr e2=expr)  -> podziel(p1={$e1.st}, p2={$e2.st})
        | ^(PODST i1=ID  e2=expr) {globals.setSymbol($i1.text, getInt($e2.text));} -> przypisanie(n={$i1.text}, p1={$e2.st})
        | ID {getSymbol($ID.text);} -> wczytaj(n={$ID.text})
        | ^(IF    e1=expr e2=expr e3=expr?) {if_cnt++;} -> if_st(p1={$e1.st}, p2={$e2.st}, p3={$e3.st}, cnt={if_cnt.toString()})
        | INT  {numer++;}           -> int(i={$INT.text},j={numer.toString()})
    ;
    