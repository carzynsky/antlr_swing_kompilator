package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {

	protected GlobalSymbols globals = new GlobalSymbols();
	
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	public void addNewGlobal(String text) {
		if(!globals.hasSymbol(text))
			globals.newSymbol(text);
	}
	
	public Integer getGlobal(String text) {
		try {
			return globals.getSymbol(text);
		}
		catch(RuntimeException ex) {
			System.err.println(ex);
			return null;
		}
	}
	
	public void setGlobal(String text, Integer value) {
		if(globals.hasSymbol(text))
			globals.setSymbol(text, value);
	}
	
	public void errorId(RuntimeException ex, CommonTree id) {
		System.err.println(ex.getMessage() + " in line " + id.getLine());
	}
}
