tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (decl | set | print)*;

// print result
print   :
          ^(PRINT e=expr) {drukuj ($e.text + " = " + $e.out.toString());}
          | e=expr {drukuj ($e.text);}
        ;
        
// declare global variable
decl    : 
          ^(VAR i1=ID) {addNewGlobal($ID.text);}
        ;
  catch [RuntimeException ex] {errorId(ex, $i1);}

// set global variable
set    : 
          ^(PODST i1=ID e2=expr) {setGlobal($i1.text, $e2.out);}
        ;
  catch [RuntimeException ex] {errorId(ex, $i1);}
  
  
expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = $e1.out +  $e2.out;}
        | ^(MINUS e1=expr e2=expr) {$out = $e1.out -  $e2.out;}
        | ^(MUL   e1=expr e2=expr) {$out = $e1.out *  $e2.out;}
        | ^(DIV   e1=expr e2=expr) {$out = $e1.out /  $e2.out;}
        | ^(MOD   e1=expr e2=expr) {$out = $e1.out \% $e2.out;}
        | ^(HI    e1=expr e2=expr) {$out = $e1.out >  $e2.out ? 1 : 0;}
        | ^(LO    e1=expr e2=expr) {$out = $e1.out <  $e2.out ? 1 : 0;}
        | ^(HIEQ  e1=expr e2=expr) {$out = $e1.out >= $e2.out ? 1 : 0;}
        | ^(LOEQ  e1=expr e2=expr) {$out = $e1.out <= $e2.out ? 1 : 0;}
        | ^(EQ    e1=expr e2=expr) {$out = $e1.out == $e2.out ? 1 : 0;}
        | ^(NE    e1=expr e2=expr) {$out = $e1.out != $e2.out ? 1 : 0;}
        | ID                       {$out = getGlobal($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        ;
